
#include "test.h"
#include <linux/mman.h>


static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void test_1(){
    printf("Test 1 starting....\n");
    void * heap = heap_init(1024);
    if (!heap){
        printf("Error in 1 test when heap_init executing\n");
        return;
    }
    void * mem = _malloc(1024);
    if (!mem){
        printf("Error in 1 test when malloc executing\n");
        return;
    }

    _free(mem);

    munmap(heap,2048);

    printf("Test 1 passed\n");
}
void test_2(){
    printf("Test 2 starting....\n");

    void * heap = heap_init(1024);

    if (!heap){
        printf("Error in 2 test when heap_init executing\n");
        return;
    }
    void * mem1 = _malloc(100);
    void * mem2 = _malloc(100);
    void * mem3 = _malloc(100);

    if (!mem1 || !mem2 || ! mem3){
        printf("Error in 2 test when malloc executing\n");
        return;
    }
    debug_heap(stdout, heap);
    _free(mem2);
    debug_heap(stdout, heap);

    _free(mem1);
    _free(mem3);
    munmap(heap,2048);
    printf("Test 2 passed\n");

}
void test_3(){
    printf("Test 3 starting....\n");
    void * heap = heap_init(1024);

    if (!heap){
        printf("Error in 3 test when heap_init executing\n");
        return;
    }
    void * mem1 = _malloc(100);
    void * mem2 = _malloc(100);
    void * mem3 = _malloc(100);

    if (!mem1 || !mem2 || ! mem3){
        printf("Error in 3 test when malloc executing\n");
        return;
    }
    debug_heap(stdout, heap);
    _free(mem2);
    _free(mem3);
    debug_heap(stdout, heap);
    _free(mem1);
    munmap(heap,2048);
    printf("Test 3 passed\n");
}

void test_4(){
    printf("Test 4 starting....\n");
    void * heap = heap_init(1024);


    if (!heap){
        printf("Error in 4 test when heap_init executing\n");
        return;
    }
    void * overflow_mem = _malloc(2048);
    debug_heap(heap,stdout);
    if (overflow_mem){
        printf("Test 4 passed\n");
        _free(overflow_mem);
        munmap(heap,2048);
        return;
    }
    printf("Test 4 failed\n");
    munmap(heap,2048);
}
void test_5(){
    printf("Test 5 starting....\n");
    void * heap = heap_init(1024);
    void * mem = _malloc(256);

    if (!heap){
        printf("Error in 5 test when heap_init executing\n");
        return;
    }
    if (!mem){
        printf("Error in 5 test when malloc executing\n");
        return;
    }
    void* map = mmap((uint8_t*)heap + 1024, 1024, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | MAP_FIXED, -1, 0);
    if (map == MAP_FAILED){
        printf("Error in 5 test when mmap executing\n");
        return;
    }

    void* mem2 = _malloc(2048);
    if (mem2 == NULL){
        printf("Error in 5 test when _malloc executing\n");
        return;
    }

    _free(mem);
    _free(mem2);
    munmap(map, 1024);

    printf("Test 5 passed\n");


}
